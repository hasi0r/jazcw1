package servlets;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.codec.Base64.InputStream;
import com.itextpdf.text.pdf.codec.Base64.OutputStream;
import com.itextpdf.tool.xml.XMLWorkerHelper;

@WebServlet("/raty")
public class RatyServlet extends HttpServlet {

	
	double kwota,oprocentowanie,ileRat,oplataStala;
	String  rodzaj,typDok;
	/**
	 * e
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		try {
			kwota = Double.parseDouble(request.getParameter("kwota"));
			oprocentowanie = Double.parseDouble(request.getParameter("oprocentowanie"));
			ileRat = Integer.parseInt(request.getParameter("ileRat"));
			oplataStala = Double.parseDouble(request.getParameter("oplataStala"));
			}catch (NumberFormatException e){
				response.setContentType("text/html");
				response.getWriter().print("<h1>Prosze sprawdzic wprowadzone dane,<br /> kwota,oprocentowanie, ilosc rat oraz stala musza byc liczbami(prosz uzyc kropki)<br /> ilosc rat musi byc liczba calkowita</h1>");
			}
		rodzaj = request.getParameter("rodzaj");
		typDok = request.getParameter("typDok");
		
		try{
			if(typDok.equals("html"))
			{
				utworzDokHTML(response, request);
			}else{
				utworzDokHTMLPDF(response, request);
			}	
		}catch(Exception e){
			response.setContentType("text/html");
			response.getWriter().print("<h1>Prosze wybrac rodzaj rat oraz rodzaj prezentacji</h1>");

		}
	}
	public void utworzDokHTML(HttpServletResponse response, HttpServletRequest request) throws IOException{
	    Document document = new Document();
	    PdfPTable table = new PdfPTable(5);
		response.setContentType("text/html");
		response.getWriter().print("<h1>Wyliczenie rat: </h1>"
				+ "<p>Wartosc kredytu: " 
				+ kwota 
				+ ", oprocentowanie "
				+ oprocentowanie
				+ ", ilosc rat: "
				+ ileRat
				+ ",rodzaj rat: raty");
			if (rodzaj.equals("malejaca")){
				response.getWriter().print(" malejaca.");
			}
			else{
				response.getWriter().print(" stala.</p>");
			}
			//Creating first row of table
			response.getWriter().print("<table border=1><tr><td>Numer raty</td><td>Kwota kapitalu</td><td>Kwota odesetek</td><td>Oplaty stale</td>"
						+ "<td>Calkowita kwota raty</td></tr>");
			//Counting of debt
			if(rodzaj.equals("stala")){  //equal installments
				liczStaleRaty("html",
						kwota, 
						oprocentowanie,
						ileRat, 
						oplataStala, document, table, response);
			}
			else if(rodzaj.equals("malejaca")){ //decreasing installments
				liczMalejaceRaty("html",
						kwota, 
						oprocentowanie,
						ileRat, 
						oplataStala, document, table, response);
			}
			response.getWriter().println("</tr></table>");
	}
	public void utworzDokHTMLPDF(HttpServletResponse response, HttpServletRequest request) throws IOException{
		response.setContentType("application/pdf");
	    String naglowek = "<h1>Wyliczenie rat dla kredytu: </h1>";
	    Document document = new Document();
	    PdfPTable table = new PdfPTable(5);
	    ByteArrayInputStream inputStream = new ByteArrayInputStream(naglowek.getBytes());
	    try {
			PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());
		    document.open();
		    XMLWorkerHelper.getInstance().parseXHtml(writer, document, inputStream);
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    String subtitle = "Kredyt: " 
	    		+ kwota 
				+ ", oprocentowanie: "
				+ oprocentowanie 
				+ ", ilosc rat: "
				+ ileRat
				+ ", raty "
				+ rodzaj
	    		+ ".";
	    Paragraph p = new Paragraph(subtitle);
	    try {
			document.add(p);
			document.add(Chunk.NEWLINE);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	    table.setHeaderRows(1);
	    table.setSplitRows(false);
	    table.setComplete(false);
	    
	    table.addCell("Numer raty");
	    table.addCell("Kwota kapitalu");
	    table.addCell("Kwota odsetek");
	    table.addCell("Oplaty stale");
	    table.addCell("Calkowita kwota raty");
	    try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		if(rodzaj.equals("stala")){  //equal installments
			liczStaleRaty("pdf", 
					kwota, 
					oprocentowanie,
					ileRat, 
					oplataStala,
					document,
					table,
					response);
		}
		else if(rodzaj.equals("malejaca")){ //decreasing installments
			liczMalejaceRaty("html", 
					kwota, 
					oprocentowanie,
					ileRat, 
					oplataStala,
					document,
					table,
					response);
		}
		
	    table.setComplete(true);
	    try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	    document.close();
	}	
	public void liczStaleRaty(String rodzaj, double kwota, double oprocentowanie, double ileRat, 
			double oplataStala, Document document, PdfPTable table, HttpServletResponse response){
		DecimalFormat df = new DecimalFormat("#.##");
		double y = 1.0 + (oprocentowanie/100.0)/12.0;
		
		double oplata = (kwota*oplataStala/100.0)/ileRat; 	
		double reszta = kwota;
		double kwotaOdsetek,kwotaKapitalu;
		double pelnaRata = kwota * Math.pow(y, ileRat) * (y-1.0)/(Math.pow(y, (ileRat))-1.0);
		double pelnaRataWy = pelnaRata +oplata;
		
		for(int i=1;i <= ileRat;i++){ 	
			kwotaOdsetek = reszta*(oprocentowanie/100.0)*(1.0/12.0);
			kwotaKapitalu = pelnaRata - kwotaOdsetek;
		
			reszta -= kwotaKapitalu;
			if(typDok.equals("html")){
				try {
					tworzRekordHTML(response,i,kwotaKapitalu,kwotaOdsetek,oplata,pelnaRataWy);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}else if (typDok.equals("pdf")){
				tworzRekordPDF(document, table, df, i, kwotaKapitalu, kwotaOdsetek, 
						oplata, pelnaRata);
			}
		}
	}
	public void liczMalejaceRaty(String rodzaj, double kwota, double oprocentowanie, double ileRat, 
			double oplataStala, Document document, PdfPTable table, HttpServletResponse response){
		DecimalFormat df = new DecimalFormat("#.##");
		double kwotaKapitalu = kwota/ileRat;
		double reszta = kwota;
		double oplata = (kwota*oplataStala/100.0)/ileRat;
		double kwotaOdsetek, pelnaRata;
		for(int i=1;i <= ileRat;i++){
			kwotaOdsetek = reszta*(oprocentowanie/100.0)*(1.0/12.0);
			reszta -= kwotaKapitalu;
			pelnaRata = kwotaKapitalu+kwotaOdsetek+oplata;
			if(typDok.equals("html")){
				try {
					tworzRekordHTML(response,i,kwotaKapitalu,kwotaOdsetek,oplata,pelnaRata);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else if (typDok.equals("pdf")){
				tworzRekordPDF(document, table, df, i, kwotaKapitalu, kwotaOdsetek, 
						oplata, pelnaRata);
			}
		}
	}
	public void tworzRekordHTML(HttpServletResponse response, int i, double kwotaKapitalu, double kwotaOdsetek, 
			double oplata, double pelnaRata) throws IOException{
			response.getWriter().print("<tr><td>"
				+ i 
				+"</td><td>"
				+ String.format("%.2f", kwotaKapitalu)
				+ "</td><td>"
				+ String.format("%.2f", kwotaOdsetek)
				+ "</td><td>"
				+ String.format("%.2f", oplata)
				+ "</td><td>"
				+ String.format("%.2f", pelnaRata)
				+ "</td>");
		}
	public void tworzRekordPDF(Document document, PdfPTable table, DecimalFormat df, int i, double kwotaKapitalu, double kwotaOdsetek, 
			double oplata, double pelnaRata){
		table.addCell(String.valueOf(i));
		table.addCell(df.format(kwotaKapitalu));
		table.addCell(df.format(kwotaOdsetek));
		table.addCell(df.format(oplata));
		table.addCell(df.format(pelnaRata));
		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
 	
}
